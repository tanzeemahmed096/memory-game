const boxes = document.querySelectorAll(".box");
const startBtn = document.querySelector(".start-btn");
const resetBtn = document.querySelector(".reset-btn");
const moves = document.querySelector(".moves");
const timer = document.querySelector(".timer");
const frontFace = document.querySelectorAll(".front-face");
const boxContainer = document.querySelector(".box-container");
const matchedBoxes = document.querySelectorAll(".matched");
let matchCount = 0;

let isFlippedBox = false;
let lockBoard = false;
let countMoves = 0;
let firstBox, secondBox;
let time = 0;
let interval;

startBtn.addEventListener("click", () => {
  interval = setInterval(() => {
    time++;
    timer.innerHTML = `Time: ${time} sec`;
  }, 1000);

  for (let idx = 0; idx < frontFace.length; idx++) {
    frontFace[idx].style.transform = "rotateY(180deg)";
    boxes[idx].style.transformStyle = "preserve-3d";
  }

  boxContainer.addEventListener("click", flipped);
  (function shuffleBoxes() {
    boxes.forEach((box) => {
      let randomPos = Math.floor(Math.random() * 16);
      box.style.order = randomPos;
    });
  })();
});

resetBtn.addEventListener("click", resetBoard);

function flipped(e) {
  const box = e.target.closest(".box");
  if (lockBoard) return;
  if (!box) return;
  if (box === firstBox) return;
  if (box.classList.contains("matched")) return;

  countMoves++;
  moves.innerHTML = `${countMoves} Moves`;
  box.classList.toggle("flip");

  if (!isFlippedBox) {
    isFlippedBox = true;
    firstBox = box;
    return;
  } else {
    isFlippedBox = false;
    secondBox = box;

    isMatched();
  }
}

function isMatched() {
  if (firstBox.dataset.value === secondBox.dataset.value) {
    firstBox.style.position = "static";
    secondBox.style.position = "static";
    firstBox.classList.add("matched");
    secondBox.classList.add("matched");
    matchCount++;
    if (matchCount === boxes.length / 2) resetFullBoard();
  } else {
    lockBoard = true;
    setTimeout(() => {
      firstBox.classList.remove("flip");
      secondBox.classList.remove("flip");
      lockBoard = false;
    }, 1500);
  }
}

function resetBoard() {
  window.location.href = "./index.html";
}

function resetFullBoard() {
  clearInterval(interval);

  setTimeout(() => {
    window.location.href = "./index.html";
  }, 5000)
}